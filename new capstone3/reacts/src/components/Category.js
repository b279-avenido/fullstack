import { Card, Image, Text, Badge, Button, Group } from '@mantine/core';
import { Row, Col, Carousel } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Category() {
  return (
    <Carousel>
      <Carousel.Item>
        <Row className="d-flex justify-content-center">   
          <h1 className='text-center'>SHOP BY CATEGORY</h1>
          <Col xs={12} md={2}>
            <Card className='xl-5' shadow="sm" padding="lg" radius="md" withBorder>
              <Card.Section>
                <Image
                  src="https://lh3.googleusercontent.com/drive-viewer/AFGJ81oTzXF0m0lVERN9z9IBe2IwxUbaP1Ti3wbDX4wqESsRcR3adDoMqaAxAuCdILFrM3-3apG-LFpDFnrvPxgGo3tUH3LhSg=s1600"
                  height={160}
                  alt="Smartphones"
                />
              </Card.Section>
              <Group position="center" mt="md" mb="xs">
                <Link className="btn btn-primary" to={`/Smartphones`}>
                  Smartphones
                </Link>
             </Group>
            </Card>
          </Col>

          <Col xs={12} md={2}>
            <Card className='xl-5' shadow="sm" padding="lg" radius="md" withBorder>
              <Card.Section>
                <Image
                  src="https://lh3.googleusercontent.com/drive-viewer/AFGJ81pwuz9Hz7NxEHnWwAckRYWo1CF2KFrqlRdk5uQMO4QoEYCJUcxJ3EBU8F-19Vmmx7J7dOvDTYeRZCCe6V-uo3mfIFJSGQ=s1600"
                  height={160}
                  alt="Laptops"
                />
              </Card.Section>
              <Group position="center" mt="md" mb="xs">
                <Link className="btn btn-primary" to={`/Laptops`}>
                  Laptops
                </Link>
              </Group>
            </Card>
          </Col>

          <Col xs={12} md={2}>
            <Card className='xl-5' shadow="sm" padding="lg" radius="md" withBorder>
              <Card.Section>
                <Image
                  src="https://lh3.googleusercontent.com/drive-viewer/AFGJ81qckQ5F2hbyTSs6qPg1bgLy367J-VGYdylZzqyf4JjCENy58STPf4dr127t-_f9ttQBSY3RBsnqB3uOjHkjlQdF5l7r=s1600"
                  height={160}
                  alt="Smartwatches"
                />
              </Card.Section>
              <Group position="center" mt="md" mb="xs">
                <Link className="btn btn-primary" to={`/Smartwatches`}>
                  Smartwatches
                </Link>
              </Group>
            </Card>
          </Col>

          <Col xs={12} md={2}>
            <Card className='xl-5' shadow="sm" padding="lg" radius="md" withBorder>
              <Card.Section>
                <Image
                  src="https://lh3.googleusercontent.com/drive-viewer/AFGJ81qL6RSKa_fxv5Ifi3GzCiFNYuz0-ytp6WtPL1B5Ud_DCxP4cO-BapPbHeBAaqlozN9dF58jSU0NKY5ojlCgpLicGSB3=s1600"
                  height={160}
                  alt="Headphones"
                />
              </Card.Section>
              <Group position="center" mt="md" mb="xs">
                <Link className="btn btn-primary" to={`/Headphones`}>
                  Headphones
                </Link>
                </Group>
            </Card>
              </Col>

          <Col xs={12} md={2}>
            <Card className='xl-5' shadow="sm" padding="lg" radius="md" withBorder>
              <Card.Section>
                <Image
                  src="https://lh3.googleusercontent.com/drive-viewer/AFGJ81qUgStpEd1_lWGjIjJ56K4MDldIihrM3tSYlwWdJ92p1PI6X2V1P64KUUgvqjaSS8bl8fJPsfUn-L0wnBLTqgz4z3QOgA=s1600"
                  height={160}
                  alt="Gaming Consoles"
                />
              </Card.Section>
              <Group position="center" mt="md" mb="xs">
                <Link className="btn btn-primary" to={`/GamingConsoles`}>
                  Gaming Consoles
                </Link>
              </Group>
            </Card>
          </Col>

        </Row> 
      </Carousel.Item>
    </Carousel>
  );
}