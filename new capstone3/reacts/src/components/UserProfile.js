import React, { useState, useEffect } from "react";
import { Container, Card, Modal, Form } from "react-bootstrap";
import { Table } from "@mantine/core";
import Swal from "sweetalert2";

export default function UserProfile() {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });

        if (!response.ok) {
          throw new Error(response.status);
        }

        const data = await response.json();

        // Check if the data returned from the API is an array, and if not, wrap it in an array
        const usersArray = Array.isArray(data) ? data : [data];

        setUsers(usersArray);
        setLoading(false);
      } catch (error) {
        console.error(error);
        Swal.fire({
          title: "Something Went Wrong!",
          icon: "error",
          text: "Please try again.",
        });
        setLoading(false);
      }
    };

    fetchUsers();
  }, []);


  return (
<Container>
      <Card className="mt-4">
        <Card.Header className="d-flex justify-content-between align-items-center">
          <h3>User Management</h3>
        </Card.Header>
        <Card.Body>
          {loading ? (
            <p>Loading...</p>
          ) : (
            <Table striped bordered hover responsive className="custom-table">
              <thead>
                <tr>
                  <th>User ID</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Admin</th>
                  <th>Verified</th>
                  <th>Balance</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {users.map(({ _id, username, email, isAdmin, verified, balance }) => (
                  <tr key={_id}>
                    <td>{_id}</td>
                    <td>{username}</td>
                    <td>{email}</td>
                    <td>{isAdmin?.toString()}</td>
                    <td>{verified?.toString()}</td>
                    <td>{balance}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          )}
        </Card.Body>
      </Card>
    </Container>
  );
}