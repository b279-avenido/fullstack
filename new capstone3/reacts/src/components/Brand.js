

import { Card, Image, Text, Badge, Button, Group } from '@mantine/core';
import {Row, Col} from "react-bootstrap"
import { Link } from "react-router-dom";

export default function HomeCarousel() {
return (
  
<Row className="d-flex justify-content-center">   
<h1 className='text-center'>Shop by Brands</h1>
    <Col xs={12} md={2}>
        <Card className='xl-5' shadow="sm" padding="lg" radius="md" withBorder>
        <Card.Section>
            <Image
            src="https://images.unsplash.com/photo-1527004013197-933c4bb611b3?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=720&q=80"
            height={160}
            alt="Norway"
            />
        </Card.Section>
            <Group position="center" mt="md" mb="xs">
                <Link className="btn btn-primary mt-3" to={`/Smartphones`}>
                Smartphones
                </Link>
            </Group>
        </Card>
    </Col>


    <Col xs={12} md={2}>
        <Card className='xl-5' shadow="sm" padding="lg" radius="md" withBorder>
        <Card.Section>
            <Image
            src="https://images.unsplash.com/photo-1527004013197-933c4bb611b3?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=720&q=80"
            height={160}
            alt="Norway"
            />
        </Card.Section>
            <Group position="center" mt="md" mb="xs">
                <Link className="btn btn-primary mt-3" to={`/Laptops`}>
                Laptops
                </Link>
            </Group>
        </Card>
    </Col>


    <Col xs={12} md={2}>
        <Card className='xl-5' shadow="sm" padding="lg" radius="md" withBorder>
        <Card.Section>
            <Image
            src="https://images.unsplash.com/photo-1527004013197-933c4bb611b3?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=720&q=80"
            height={160}
            alt="Norway"
            />
        </Card.Section>
            <Group position="center" mt="md" mb="xs">
                <Link className="btn btn-primary mt-3" to={`/Smartwatches`}>
                Smartwatches
                </Link>
            </Group>
        </Card>
    </Col>


    <Col xs={12} md={2}>
        <Card className='xl-5' shadow="sm" padding="lg" radius="md" withBorder>
        <Card.Section>
            <Image
            src="https://images.unsplash.com/photo-1527004013197-933c4bb611b3?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=720&q=80"
            height={160}
            alt="Norway"
            />
        </Card.Section>
            <Group position="center" mt="md" mb="xs">
                <Link className="btn btn-primary mt-3" to={`/Headphones`}>
                Headphones
                </Link>
            </Group>
        </Card>
    </Col>


    <Col xs={12} md={2}>
        <Card className='xl-5' shadow="sm" padding="lg" radius="md" withBorder>
        <Card.Section>
            <Image
            src="https://images.unsplash.com/photo-1527004013197-933c4bb611b3?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=720&q=80"
            height={160}
            alt="Norway"
            />
        </Card.Section>
            <Group position="center" mt="md" mb="xs">
                <Link className="btn btn-primary mt-3" to={`/GamingConsoles`}>
                Gaming Consoles
                </Link>
            </Group>
        </Card>
    </Col>
</Row> 
);
}
