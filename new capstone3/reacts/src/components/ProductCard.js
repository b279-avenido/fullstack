import { Card, Image, Text, Badge, Button, Group, Container, Grid } from "@mantine/core";
import { Link } from "react-router-dom";
import Highlights from "./Highlights";

export default function ProductCard({ productProp }) {
  const { _id, name, description, category, price, image } = productProp;

  return (
    <Container className="mt-5" fluid>
      <Grid xs={1} sm={2} md={3}>
        <Card shadow="sm" padding="lg" radius="md" withBorder xs={{ width: "100%" }}>
          <Card.Section>
            <Image src={image} height={300} alt="Norway" />
          </Card.Section>

          <Group position="apart" mt="md" mb="xs">
            <Text weight={500}>{name}</Text>
            <Badge color="pink" variant="light">
              {category}
            </Badge>
          </Group>

          <Text size={{ xs: "sm", md: "md" }} color="dimmed">
            {description}
          </Text>

          <Text
            fw={700}
            Bold
            size={{ xs: "sm", md: "lg" }}
            className="mt-2"
            color="pink"
            style={{ fontSize: "1.2rem", marginTop: "3rem" }}
          >
            ₱ {price}
          </Text>

          <Link
            className="btn btn-primary mt-3 text-center w-100"
            to={`/productView/${_id}`}
          >
            Details
          </Link>
        </Card>
      </Grid>
    </Container>
  );
}
