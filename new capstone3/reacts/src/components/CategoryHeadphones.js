import ProductCard from './ProductCard'
import { useEffect, useState} from "react"
import { SimpleGrid } from '@mantine/core';

export default function Headphones(){
	const [AllHeadphones, setAllHeadphones] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/category/Headphones`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setAllHeadphones(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product}/>

					)
			}))
		})
	}, [])

	return(
		<>
			<h1>Headphones</h1>
			<SimpleGrid cols={3}>
			{/*Prop making ang prop passing*/}
			{AllHeadphones}
			</SimpleGrid>
		</>
		)
}