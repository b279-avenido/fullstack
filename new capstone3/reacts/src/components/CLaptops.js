import productData from "../data/products"
import ProductCard from './ProductCard'
import Highlights from './Highlights'
import { useEffect, useState} from "react"


export default function Laptops(){
	const [AllLaptops, setAllLaptops] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/category/Laptops`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setAllLaptops(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product}/>

					)
			}))
		})
	}, [])

	return(
		<>
			<h1>Laptops</h1>
			{/*Prop making ang prop passing*/}
			{AllLaptops}
		</>
		)
}