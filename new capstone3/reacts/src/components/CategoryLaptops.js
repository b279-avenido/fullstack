import ProductCard from './ProductCard'
import { useEffect, useState} from "react"
import { SimpleGrid } from '@mantine/core';

export default function Laptops(){
	const [AllLaptops, setAllLaptops] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/category/Laptops`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setAllLaptops(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product}/>

					)
			}))
		})
	}, [])

	return(
		<>
			<h1>Laptops</h1>
			<SimpleGrid cols={3}>
			{/*Prop making ang prop passing*/}
			{AllLaptops}
			</SimpleGrid>
		</>
		)
}