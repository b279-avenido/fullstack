
import { useState, useEffect, useContext } from "react";
import { Container, Card, Image, Grid, Text, Badge, Group, Button } from "@mantine/core";
import { useParams, Link } from "react-router-dom";
import { Row, Col } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { SimpleGrid } from '@mantine/core';
import Highlights from "./Highlights";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function ProductView() {
  const { user } = useContext(UserContext);

  // module that allows us to retrieve the productId passed via URL
  const { productId } = useParams();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [category, setCategory] = useState("");
  const [image, setImage] = useState("");
  const [quantity, setQuantity] = useState(1); // new state variable for quantity
  const navigate = useNavigate();
  
  const handleCheckout = () => {
	// Show the SweetAlert
	Swal.fire({
	  title: "Redirecting to Login",
	  icon: "error",
	  text: "Thank you for your cooperation",
	}).then(() => {
	  // Store the login state in local storage
	  localStorage.setItem("isLoggedIn", "false");
  
	  // Navigate to the login page
	  navigate(`/login`);
	});
  };
  useEffect(() => {
    console.log(productId);

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
		setCategory(data.category);
		setImage(data.image);
      });
  }, [productId]);

  const incrementQuantity = () => {
    if (quantity < 10) {
      setQuantity(quantity + 1);
    }
  };

  const decrementQuantity = () => {
    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  }

  const checkout = (productId, quantity) => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/save`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        items: [
          {
            productId: productId,
            quantity: quantity,
          },
        ],
      }),
    })
      .then((res) => {
        if (!res.ok) {
          throw new Error(res.status);
        }
        return res.json();
      })
      .then((data) => {
		console.log("Checkout response data:", data);
	
		Swal.fire({
			icon: 'success',
			title: 'Order Succesfuly added',
			showDenyButton: true,
			showCancelButton: true,
			confirmButtonText: 'Go to Products',
			denyButtonText: `Go to my Cart`,
		  }).then((result) => {
			if (result.isConfirmed) {
			  navigate(`/products`);
			} else if (result.isDenied) {
			  navigate(`/orders/myOrders`);
			}
		  })
		})

	  .catch((error) => {
        console.error(error);
        Swal.fire({
          title: "Something Went Wrong!",
          icon: "error",
          text: "Please try again.",
        });
      });
  };
console.log(`Image ${image}`)
  return (
	<Container className="mt-5">
	<Grid>
	<Row className="d-flex justify-content-center">
	<h1 className='text-center mt-5'>PRODUCTS</h1>
		<Col xs={12} md={10}>
		<Card className='xl-5' shadow="sm" padding="lg" radius="md" withBorder>
		<Card.Section>
			<Image
			src={image}
			height={420}
			alt="Norway"
			/>
		</Card.Section>

		<Group position="apart" mt="md" mb="xs">
			<Text weight={500} style={{ fontSize: "2.8rem"}}>{name}</Text>
			<Badge color="pink" variant="light">
			{category}
			</Badge>
		</Group>

		<Text size="sm" color="dimmed" style={{ fontSize: "1.5rem"}}>
		{description}
		</Text>

		<Text fw={700}Bold size="sm" className="mt-2" color="pink" style={{ fontSize: "2rem", marginTop: "3rem" }}>
			₱ {price}
		</Text>

		<Text align="center" style={{ fontSize: "1.5rem", marginTop: "2rem" }}>
				Quantity
			  </Text>
			  <div
				style={{
				  display: "flex",
				  justifyContent: "center",
				  alignItems: "center",
				}}
			  >
				<Button variant="light" onClick={decrementQuantity}>
				  -
				</Button>
				<Text style={{ margin: "0 1rem" }}>{quantity}</Text>
				<Button variant="light" onClick={incrementQuantity}>
				  +
				</Button>
			  </div>
			<Card.Section className="mt-2 "
			  style={{
				display: "flex",
				justifyContent: "center",
				marginTop: "1rem",
				marginBottom: "1rem",
			  }}
			>
				{localStorage.token ? (
				<Button 
					variant="gradient"
					gradient={{ from: "indigo", to: "cyan" }}
					onClick={() => checkout(productId, quantity)}
					size="lg"
				>
					Add to cart
				</Button>
				) : (
				<Button
					variant="gradient"
					gradient={{ from: "teal", to: "lime", deg: 105 }}
					onClick={handleCheckout}
					size="lg"
				>
					LOGIN TO BUY
				</Button>
				)}
			</Card.Section>
			</Card>
		</Col>
		
	  </Row>
	  
	</Grid>	
  </Container>
  );
}
