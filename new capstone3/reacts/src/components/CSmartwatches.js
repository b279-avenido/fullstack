import ProductCard from './ProductCard'
import { useEffect, useState} from "react"


export default function Smartwatches(){
	const [AllSmartwatches, setAllSmartwatches] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/category/Smartwatches`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setAllSmartwatches(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product}/>

					)
			}))
		})
	}, [])

	return(
		<>
			<h1>Smartwatches</h1>
			{/*Prop making ang prop passing*/}
			{AllSmartwatches}
		</>
		)
}