import { useEffect, useState} from "react"
import ProductCard from './ProductCard'
import { SimpleGrid } from '@mantine/core';


export default function GamingConsole(){
	const [AllGamingConsole, setAllGamingConsole] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/category/GamingConsoles`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setAllGamingConsole(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product}/>
					)
			}))
		})
	}, [])

return(
	<>
		<h1>GamingConsole</h1>
		<SimpleGrid cols={3}> 
		{/*Prop making ang prop passing*/}
		{AllGamingConsole}
		</SimpleGrid>
	</>
	)
}