import ProductCard from './ProductCard'
import { useEffect, useState} from "react"
import { SimpleGrid } from '@mantine/core';

export default function Highlights(){
  const [AllTrending, setAllTrending] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/trending/true`)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      setAllTrending(data.map(product => {
        return (
          <ProductCard key={product._id} productProp={product}/>

          )
      }))
    })
  }, [])

  return(
    
    <>
      <h1>Trending</h1>
      <SimpleGrid cols={3}>
      {/*Prop making ang prop passing*/}
      {AllTrending}
      </SimpleGrid>
    </>
    )
}