import productData from "../data/products"
import ProductCard from './ProductCard'
import Highlights from './Highlights'
import { useEffect, useState} from "react"


export default function Smartphones(){
	const [AllSmartphones, setAllSmartphones] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/category/Smartphones`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setAllSmartphones(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product}/>

					)
			}))
		})
	}, [])

	return(
		<>
			<h1>Smartphones</h1>
			{/*Prop making ang prop passing*/}
			{AllSmartphones}
		</>
		)
}