import { Col } from "react-bootstrap";
import { Card, Text, Badge, Group } from "@mantine/core";
import { Link } from "react-router-dom";

export default function OrderCard({ orderProp }) {
  if (!orderProp) {
    return null; // or you can return a default UI component to indicate that the data is still loading
  }

  const { _id, name, description, price } = orderProp;

  return (
    <Col xs={12} md={6} lg={4} className="mt-3 mb-3">
      <Card shadow="sm" radius="md" withBorder>
        <Group position="apart" mt="md" mb="xs">
          <Text weight={500}>{name}</Text>
          <Badge color="pink" variant="light">
            On Sale
          </Badge>
        </Group>
        <Text size="sm" color="dimmed">
          {description}
        </Text>
        <Text size="sm" color="dimmed">
          {price}
        </Text>
        <Link className="btn btn-primary" to={`/orders/${_id}`}>
          Details
        </Link>
      </Card>
    </Col>
  );
}