import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import Category from "../components/Category";
import Products from "./Products";
import "./Pages.css"; // Import the CSS file

export default function Home(){


    return(
        <div className="p-1">
        <div className="home-background">

        </div>
            <div>
                <Category/>
                <Highlights/>
                <Products/>
            </div>
        </div>
    )
}