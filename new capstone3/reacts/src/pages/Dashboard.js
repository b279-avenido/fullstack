import { useState, useEffect } from "react";
import { Container, Card, Modal, Form } from "react-bootstrap";
import { Table, Button } from "@mantine/core";
import Swal from "sweetalert2";

export default function ProductManagement() {
  const [products, setProducts] = useState([]);
  const [showEditModal, setShowEditModal] = useState(false);
  const [showAddModal, setShowAddModal] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState({});

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });

        if (!response.ok) {
          throw new Error(response.status);
        }

        const data = await response.json();
        setProducts(data);
      } catch (error) {
        console.error(error);
        Swal.fire({
          title: "Something Went Wrong!",
          icon: "error",
          text: "Please try again.",
        });
      }
    };

    fetchProducts();
  }, []);

  const handleDelete = async (productId) => {
    try {
      const result = await Swal.fire({
        title: "Are you sure?",
        text: "This product will be permanently deleted!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        cancelButtonColor: "#3085d6",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "Cancel",
      });

      if (result.isConfirmed) {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/delete`, {
          method: "DELETE",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/json",
          },
        });

        if (!response.ok) {
          throw new Error(response.status);
        }

        setProducts(products.filter(product => product._id !== productId));
        Swal.fire({
          title: "Deleted!",
          text: "The product has been deleted.",
          icon: "success",
        });
      }
    } catch (error) {
      console.error(error);
      Swal.fire({
        title: "Something Went Wrong!",
        icon: "error",
        text: "Please try again.",
      });
    }
  };

  const handleAdd = () => {
    setSelectedProduct({});
    setShowAddModal(true);
  };

  const handleEdit = (product) => {
    setSelectedProduct(product);
    setShowEditModal(true);
  };

  const handleUpdate = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/add`, {
        method: "POST",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(selectedProduct),
      });

      if (!response.ok) {
        throw new Error(response.status);
      }

      const newProduct = await response.json();
      setProducts([...products, newProduct]);
      setShowAddModal(false);
      Swal.fire({
        title: "Added!",
        text: "The product has been added.",
        icon: "success",
      });
    } catch (error) {
      console.error(error);
      setShowAddModal(false);
      Swal.fire({
        title: "Something Went Wrong!",
        icon: "error",
        text: "Please try again.",
      });
    }
  };

  const handleUpdateProduct = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${selectedProduct._id}/update`, {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(selectedProduct),
      });

      if (!response.ok) {
        throw new Error(response.status);
      }

      const updatedProduct = await response.json();
      setProducts(products.map(product => product._id === updatedProduct._id ? updatedProduct : product));
      setShowEditModal(false);
      Swal.fire({
        title: "Updated!",
        text: "The product has been updated.",
        icon: "success",
      });
    } catch (error) {
      console.error(error);
      setShowEditModal(false);
      Swal.fire({
        title: "Something Went Wrong!",
        icon: "error",
        text: "Please try again.",
      });
    }
  };

  return (
<Container>
  <Card className="mt-4">
    <Card.Header className="d-flex justify-content-between align-items-center">
      <h3>Product Management</h3>
      <Button variant="gradient" gradient={{ from: "blue", to: "purple" }} onClick={() => setShowAddModal(true)}>
        Add Product
      </Button>
    </Card.Header>
    <Card.Body>
          <Table striped bordered hover responsive className="custom-table">
            <thead>
              <tr>
                <th>Product ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {products.map(({ _id, name, description, price }) => (
                <tr key={_id}>
                  <td>{_id}</td>
                  <td>{name}</td>
                  <td>{description}</td>
                  <td>{price}</td>
                  <td>
                    <Button
                      variant="gradient"
                      gradient={{ from: "orange", to: "red" }}
                      onClick={() => handleDelete(_id)}
                    >
                      Delete
                    </Button>
                    <Button
                      variant="gradient"
                      gradient={{ from: "green", to: "blue" }}
                      className="ml-2"
                      onClick={() => handleEdit({ _id, name, description, price })}
                    >
                      Edit
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Card.Body>
      </Card>

      <Modal show={showEditModal} onHide={() => setShowEditModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleUpdateProduct}>
            <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                value={selectedProduct.name}
                onChange={(event) =>
                  setSelectedProduct({ ...selectedProduct, name: event.target.value })
                }
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                value={selectedProduct.description}
                onChange={(event) =>
                  setSelectedProduct({ ...selectedProduct, description: event.target.value })
                }
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Price</Form.Label>
             <Form.Control
                type="number"
                value={selectedProduct.price}
                onChange={(event) =>
                  setSelectedProduct({ ...selectedProduct, price: event.target.value })
                }
              />
            </Form.Group>
            <Button type="submit" variant="gradient" gradient={{ from: "green", to: "blue" }}>
              Update
            </Button>
          </Form>
        </Modal.Body>
      </Modal>

      <Modal show={showAddModal} onHide={() => setShowAddModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Add Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleUpdate}>
            <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                value={selectedProduct.name || ""}
                onChange={(event) =>
                  setSelectedProduct({ ...selectedProduct, name: event.target.value })
                }
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                value={selectedProduct.description || ""}
                onChange={(event) =>
                  setSelectedProduct({ ...selectedProduct, description: event.target.value })
                }
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                value={selectedProduct.price || ""}
                onChange={(event) =>
setSelectedProduct({ ...selectedProduct, price: event.target.value })
                }
              />
            </Form.Group>
            <Button type="submit" variant="gradient" gradient={{ from: "green", to: "blue" }}>
              Add
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </Container>
  );
}