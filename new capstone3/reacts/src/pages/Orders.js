import { useEffect, useState } from "react";
import { Container, Row, Alert } from "react-bootstrap";
import OrderCard from "../components/OrderCard";

export default function Orders() {
  const [allOrders, setAllOrders] = useState([]);
  const [error, setError] = useState(null);

useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders`)
      .then(res => res.json())
      .then(data => {
        console.log(data);
  
        if (data && Array.isArray(data)) { // add this check
          setAllOrders(data.map(order => {
            return (
              <OrderCard key={order._id} orderProp={order}/>
            )
          }))
        } else {
          console.error('Data is not an array:', data);
        }
      })
  }, [])
    
  return (
    <Container>
        <h1>Orders</h1>
        {error && <Alert variant="danger">{error}</Alert>}
        
        <Row>
        {allOrders.map((order) => (
        <OrderCard key={order._id} orderProp={order} />
        ))}
        </Row>
    </Container>
  );
}