import { useState, useEffect } from "react";
import { Container, Card, Modal, Form } from "react-bootstrap";
import { Table, Button, Pagination } from "@mantine/core";
import Swal from "sweetalert2";

export default function UserManagement() {
  const [users, setUsers] = useState([]);
  const [showEditModal, setShowEditModal] = useState(false);
  const [showAddModal, setShowAddModal] = useState(false);
  const [selectedUser, setSelectedUser] = useState({});
  const [activePage, setActivePage] = useState(1);

  const PAGE_SIZE = 7;

// compute total number of pages based on number of products and page size
const totalPages = Math.ceil(users.length / PAGE_SIZE);

const startIdx = (activePage - 1) * PAGE_SIZE;
const endIdx = startIdx + PAGE_SIZE;
const displayedUser = users.slice(startIdx, endIdx);

useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });

        if (!response.ok) {
          throw new Error(response.status);
        }

        const data = await response.json();
        setUsers(data);
      } catch (error) {
        console.error(error);
        Swal.fire({
          title: "Something Went Wrong!",
          icon: "error",
          text: "Please try again.",
        });
      }
    };

    fetchUsers();
  }, []);

  const handleDelete = async (userId) => {
    try {
      const result = await Swal.fire({
        title: "Are you sure?",
        text: "This user will be permanently deleted!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        cancelButtonColor: "#3085d6",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "Cancel",
      });

      if (result.isConfirmed) {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/delete`, {
          method: "DELETE",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/json",
          },
        });

        if (!response.ok) {
          throw new Error(response.status);
        }

        setUsers(users.filter(user => user._id !== userId));
        Swal.fire({
          title: "Deleted!",
          text: "The user has been deleted.",
          icon: "success",
        });
      }
    } catch (error) {
      console.error(error);
      Swal.fire({
        title: "Something Went Wrong!",
        icon: "error",
        text: "Please try again.",
      });
    }
  };

  const handleAdd = () => {
    setSelectedUser({});
    setShowAddModal(true);
  };

  const handleEdit = (user) => {
    setSelectedUser(user);
    setShowEditModal(true);
  };

  const handleUpdate = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/users`, {
        method: "POST",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(selectedUser),
      });

      if (!response.ok) {
        throw new Error(response.status);
      }

      const newUser = await response.json();
      setUsers([...users, newUser]);
      setShowAddModal(false);
      Swal.fire({
        title: "Added!",
        text: "The user has been added.",
        icon: "success",
      });
    } catch (error) {
      console.error(error);
      setShowAddModal(false);
      Swal.fire({
        title: "Something Went Wrong!",
        icon: "error",
        text: "Please try again.",
      });
    }
  };

  const handleUpdateUser = async (event) => {
    event.preventDefault();
  
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/users/${selectedUser._id}`, {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(selectedUser),
      });
  
      if (!response.ok) {
        throw new Error(response.status);
      }
  
      const updatedUser = await response.json();
      setUsers(users.map(user => user._id === updatedUser._id ? updatedUser : user));
      setShowEditModal(false);
  
      Swal.fire({
        title: "Updated!",
        text: "The user has been updated.",
        icon: "success",
      });
    } catch (error) {
      console.error(error);
      setShowEditModal(false);
  
      if (error.response && error.response.text) {
        const responseText = await error.response.text();
        if (responseText.includes("User Updated!")) {
          Swal.fire({
            title:"Updated!",
            text: "The user has been updated.",
            icon: "success",
          });
        } else {
          Swal.fire({
            title: "ito!",
            icon: "error",
            text: "Please try again.",
          });
        }
      } else if (error.message !== "200") { // Check if response status code is not 200
        Swal.fire({
            title:"Updated!",
            text: "The user has been updated.",
            icon: "success",
          });
      }
    }
  };
  

  return (
<Container>
  <Card className="mt-4">
    <Card.Header className="d-flex justify-content-between align-items-center">
      <h3>User Management</h3>

    </Card.Header>
    <Card.Body>
          <Table striped bordered hover responsive className="custom-table">
            <thead>
            <tr>
                <th>User ID</th>
                <th>Username</th>
                <th>Email</th>
                <th>Admin</th>
                <th>Verified</th>
                <th>Balance</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            {displayedUser.map(({ _id, username, email,isAdmin, verified, balance }) => (
                <tr key={_id}>
                  <td>{_id}</td>
                  <td>{username}</td>
                  <td>{email}</td>
                  <td>{isAdmin.toString()}</td>
                  <td>{verified.toString()}</td>
                  <td>{balance}</td>
                  <td>
                    <Button
                      variant="gradient"
                      gradient={{ from: "orange", to: "red" }}
                      onClick={() => handleDelete(_id)}
                    >
                      Delete
                    </Button>
                    <Button
                      variant="gradient"
                      gradient={{ from: "green", to: "blue" }}
                      className="ml-2"
                      onClick={() => handleEdit({ _id, username, email,isAdmin, verified, balance })}
                    >
                      Edit
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Card.Body>
      </Card>

      <div className="d-flex justify-content-center">
      <Pagination
        value={activePage}
        onChange={setActivePage}
        total={totalPages}
      />
    </div>

      <Modal show={showEditModal} onHide={() => setShowEditModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Edit User</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleUpdateUser}>
            <Form.Group>
              <Form.Label>Username</Form.Label>
              <Form.Control
                type="text"
                value={selectedUser.username}
                onChange={(event) =>
                  setSelectedUser({ ...selectedUser, username: event.target.value })
                }
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="text"
                value={selectedUser.email}
                onChange={(event) =>
                  setSelectedUser({ ...selectedUser, email: event.target.value })
                }
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Balance</Form.Label>
             <Form.Control
                type="number"
                value={selectedUser.balance}
                onChange={(event) =>
                  setSelectedUser({ ...selectedUser, balance: event.target.value })
                }
              />
            </Form.Group>


            <Form.Label>Admin:</Form.Label>
              <Form.Group>
                <Form.Check
                  type="checkbox"
                  label="Available"
                  checked={selectedUser.isAdmin}
                  onChange={() => setSelectedUser({ ...selectedUser, isAdmin: true })}
                />
                <Form.Check
                  type="checkbox"
                  label="Not Available"
                  checked={!selectedUser.isAdmin}
                  onChange={() => setSelectedUser({ ...selectedUser, isAdmin: false })}
                />
              </Form.Group>


              <Form.Label>Verified:</Form.Label>
              <Form.Group>
                <Form.Check
                  type="checkbox"
                  label="Available"
                  checked={selectedUser.verified}
                  onChange={() => setSelectedUser({ ...selectedUser, verified: true })}
                />
                <Form.Check
                  type="checkbox"
                  label="Not Available"
                  checked={!selectedUser.verified}
                  onChange={() => setSelectedUser({ ...selectedUser, verified: false })}
                />
              </Form.Group>
            <Button type="submit" variant="gradient" gradient={{ from: "green", to: "blue" }}>
              Update
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </Container>
  );
}