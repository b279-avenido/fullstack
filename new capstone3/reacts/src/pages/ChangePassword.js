import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import "./Pages.css";

export default function ChangePassword() {
  const { user } = useContext(UserContext);
    //an object with methods to redirect the user
    const navigate = useNavigate();
    // State hooks to store the values of the input fields
    const {unsetUser, setUser} = useContext(UserContext);
    const [currentPassword, setCurrentPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [confirmNewPassword, setConfirmNewPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    // Check if values are successfully binded
    console.log(currentPassword);
    console.log(newPassword);
    console.log(confirmNewPassword);

    // Function to simulate password change
    function changePassword(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        // Validate new password
        if (newPassword !== confirmNewPassword) {
            Swal.fire({
                title: 'Passwords do not match',
                icon: 'error',
                text: 'Please try again.'   
            });
            return;
        }

        // Simulate password change request
        fetch(`${ process.env.REACT_APP_API_URL }/users/changepassword`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json",
              },
            body: JSON.stringify({
                currentPassword: currentPassword,
                newPassword: newPassword
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(typeof data !== "undefined"){
                // Clear input fields
                setCurrentPassword('');
                setNewPassword('');
                setConfirmNewPassword('');

                Swal.fire({
                    icon: 'success',
                    title: 'Login Successful!',
                    text: 'Welcome to Digital Depot!',
                  })
                  unsetUser();
                  navigate("/login");
              }else{
                  Swal.fire({
                    icon: 'error',
                    title: 'Authentication Failed!',
                    text: 'Please try again!',
                  })

            };

        })
    };

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and new passwords match
        if((currentPassword !== '' && newPassword !== '' && confirmNewPassword !== '') && (newPassword === confirmNewPassword)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [currentPassword, newPassword, confirmNewPassword]);

    return (
        (user.token === null) ?
            <navigate to="/login" />
        :
        <div className="change-password-container">
        <div className="image-container">
        <h1>CHANGE PASSWORD</h1>
        </div>
        <div className="form-container">
            <Form onSubmit={(e) => changePassword(e)}>
                <Form.Group controlId="currentPassword">
                    <Form.Label>Current Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Enter current password"
                        value={currentPassword} 
                        onChange={e => setCurrentPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="newPassword">
                    <Form.Label>New Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Enter new password"
                        value={newPassword} 
                        onChange={e => setNewPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="confirmNewPassword">
                    <Form.Label>Confirm New Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Confirm new password"
                        value={confirmNewPassword} 
                        onChange={e => setConfirmNewPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                { isActive ? 
                    <Button className="mt-3" variant="primary" type="submit" id="submitBtn">
                        Change Password
                    </Button>
                    : 
                    <Button className="mt-3" variant="danger" type="submit" id="submitBtn" disabled>
                        Change Password
                    </Button>
                }
            </Form>
            </div>
        </div>
    )
    
}