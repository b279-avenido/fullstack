import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useNavigate, Link, Navigate } from 'react-router-dom';
import { Code, Title } from '@mantine/core';
import UserContext from "../UserContext"
import Swal from "sweetalert2"
import "./Pages.css";

export default function Login() {
    const { user, setUser } = useContext(UserContext);
    const navigate = useNavigate();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(username !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [username, password]);

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        // Process a fetch request tothe corresponding API
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-type" : "application/json"
            },
            body: JSON.stringify({
                username : username,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

            // If no user info is found, the "access" property will not be available
            if(typeof data.access !== "undefined"){
                localStorage.setItem("token", data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                  icon: 'success',
                  title: 'Login Successful!',
                  text: 'Welcome to Digital Depot!',
                })
                navigate("/");
            }else{
                Swal.fire({
                  icon: 'error',
                  title: 'Authentication Failed!',
                  text: 'Please try again!',
                })
            }
        })

        // Clear input fields after submission
        setUsername('');
        setPassword('');
    }

     // Retrieve user details using its token
    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
                id: localStorage.setItem("id", data._id),
                isAdmin: localStorage.setItem("username", data.username),
                username: localStorage.setItem("isAdmin", data.isAdmin)
            })
        })
    }

    return (
        (user.token !==null) ?
            <Navigate to="/products"/>
        :
        <div className="login-container">
            <div className="image-container">
            <h1>LOGIN</h1>
            </div>
            <div className="form-container">
                <Form onSubmit={(e) => authenticate(e)} className="my-5">
                    <Form.Group controlId="userUsername" className="mb-3">
                        <Form.Label>Username</Form.Label>
                        <Form.Control 
                            type="username" 
                            placeholder="Enter username" 
                            required
                            value={username}
                            onChange={(e) => setUsername(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group controlId="password" className="mb-3">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            placeholder="Password" 
                            required
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </Form.Group>

                    { isActive ? 
                        <Button variant="success" type="submit" id="submitBtn" className="mt-3">
                            Login
                        </Button>
                        : 
                        <Button variant="danger" type="submit" id="submitBtn" className="mt-3" disabled>
                            Login
                        </Button>
                    }

                    <div className='d-flex flex-column align-items-center justify-content-center mt-5'>
                    <p className='mb-3'>No account yet? <Link to={"/register"}><strong>Click Here!</strong></Link></p>
                    <Title order={3}>ADMIN ACCOUNT</Title>
                    <Code>Username: ADMIN | Password: admin</Code>
                    </div>
                </Form>
           </div>
        </div>
    )
}