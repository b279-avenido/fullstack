const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
	
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	mobileNo : {
		type : String,
		required : [true, "mobile Number is required"]
	},	
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	orderedProduct: [{
		product: [{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			productName: {
				type: String
				// required: [true, "Product Name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Number is required"]
			}
		}],
		totalAmount: {
				type: Number
				// required: [true, "Amount is required"]
			},
			purchaseOn: {
				type: Date,
				default: new Date()
			}
	}]
	
});

module.exports = mongoose.model("User", userSchema);